const fs = require('fs')
const ftpClient = require('ftp')
const path = require('path')

/**
 * 配置
 * @type {[{},string]}
 */
let config = {
    /**
     * 要迁移的日志源路径及其对应的目的地目录
     */
    path:[
        {
            from:'/Users/yanwushu/Documents/code/test',
            to:'/test'
        }
    ],
    /**
     * ftp 服务器信息
     */
    ftp:{
        host:'60.214.150.242',
        user:'test',
        port:21,
        pwd:'j3LCpiRZx33fnTed'
    }
}

/**
 * 迁移 config.path 中的日志
 * 从 from 目录中迁移至 to 目录中
*/
function move(){
    let path = config.path;
    path.forEach((item)=>{
        moveOneDir(item.from,item.to)
    })
}

/**
 * 迁移一个目录中的日志文件到指定目录
 */
function moveOneDir(fromDir,toDir){

}

/**
 * 从一个目录数组中获取所有日志文件
 * @param dirList
 * @returns {*[]}
 */
function getLogFilesFromDirList(dirList){
    let r = [];
    dirList.forEach((item)=>{
        r.push(getLogFilesFromDir(item));
    });
    return r;
}

/**
 * 从一个目录中找到需要迁移的日志文件
 * 规则为文件名中带有日期的 文件，比如 log-2023-12-19.php
 * 递归查找
 */
function getLogFilesFromDir(dir){

}

/**
 * 炸
 * @param path
 * @param type string 'file' or 'dir'
 * @returns {*[]}
 */
function getAllDirsOrFiles(path,type){
    const items = fs.readdirSync(path);
    let r = [];

    // 遍历当前目录中所有的文件和文件夹
    items.map(item => {
        let temp = path.join(path, item);
        // 若当前的为文件夹
        if( fs.statSync(temp).isDirectory()){
            r.push({type:'file',path:item})
        }else if(fs.statSync(temp).isFile()){
            r.push({type:'dir',path:item})
        }else {
            console.error('无法识别是文件还是目录')
        }
    });
    return r;
}

/**
 * 找到一个目录中的所有日志文件
 * @param path
 * @returns {*[]}
 */
function getLogFiles(path='.'){
    const items = fs.readdirSync(path);
    let result = [];

    // 遍历当前目录中所有的文件和文件夹
    items.map(item => {
        let temp = path.join(path, item);

        // 若当前的为文件夹
        if( fs.statSync(temp).isDirectory() ){
            result.push( item ); // 存储当前文件夹的名字
            // 进入下一级文件夹访问
            result = result.concat( getAllDirs( temp ) );
        }
    });
    return result;
}


/**
 * 登录 ftp 并创建一个目录
 */
function createDir(){
    let c = new ftpClient();

    c.on('ready', function() {
        console.log('✅ ftp client 就绪');
    });

    c.connect({
        host: config.ftp.host,
        port: config.ftp.port,
        user: config.ftp.user,
        password: config.ftp.pwd
    });

    console.log('✅ ftp 连接成功 ');

    c.login(function(err) {
        if (err) {
            console.log('❌ftp 登录失败');
        }
        console.log('✅ ftp 登录成功');
    });


}
createDir();




